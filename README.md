# Hey There! I'm Euan 👋🏼<img src="https://emojis.slackmojis.com/emojis/images/1531849430/4246/blob-sunglasses.gif?1531849430" width="30"/>

## More About Me 
I am currently working as a Senior Digitalisation Consultant, specialising in SAP-related Digitalisation projects.  
Although my professional life revolves around the SAP ecosystem, I also have a keen interest in technologies outside of SAP - Node JS, Spring Boot, Python, R, just to name a few.
I love spending my time outside of work reading about technological advances and experimenting with or building software, especially with frameworks or tools that I don't already know. <br> I am also a math enthusiast, and I enjoy exploring topics of Mathematical Finance during my free time. I believe that the stock market is full of patterns, and I am fascinated by the prospect of finding such patterns and potentially making a profit from them. <br>
You can find some of my mini experiments and/or projects here on Github! <br> 
Let's connect and collaborate! You can find links to my LinkedIn and my email address below. Feel free to reach out to me, and let's change the world with software! 🚀



## 📫 My Details
Full Name: Euan Zheng Hou Low <br>
📍 Current Location: Kuala Lumpur, Malaysia <br>
🎓 Alma Mater: Australian National University (ANU), Canberra, Australia <br>
🌟 Interests outside work: Mathematical Finance 🗠, Basketball 🏀, Muay Thai 🥊, Cooking 🍳 <br>

## 💻 Technical Skills
### Frontend Development
<p>
  <img src="https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white" />  
  <img src="https://img.shields.io/badge/CSS-239120?style=for-the-badge&logo=css3&logoColor=white" /> 
  <img src="https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=JavaScript&logoColor=white" /> 
  <img src="https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB" />
  <img src="https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white" />
  <img src="https://img.shields.io/badge/Tailwind_CSS-38B2AC?style=for-the-badge&logo=tailwind-css&logoColor=white" />
  
</p>

### Backend Development
<p>
  <img src="https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white" />
  <img src="https://img.shields.io/badge/Express.js-404D59?style=for-the-badge" />  
  <img src="https://img.shields.io/badge/Django-092E20?style=for-the-badge&logo=django&logoColor=white" /> 
  <img src="https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white" />
</p>

### Databases
<p>
  <img src="https://img.shields.io/badge/MongoDB-4EA94B?style=for-the-badge&logo=mongodb&logoColor=white" />
  <img src="https://img.shields.io/badge/SQLite-07405E?style=for-the-badge&logo=sqlite&logoColor=white" />
  <img src="https://img.shields.io/badge/PostgreSQL-316192?style=for-the-badge&logo=postgresql&logoColor=white" /> 
</p>

### Cloud
<p>
  <img src="https://img.shields.io/badge/Cloud%20Foundry-0C9ED5?style=for-the-badge&logo=Cloud%20Foundry&logoColor=white" />
  <img src="https://img.shields.io/badge/Google_Cloud-4285F4?style=for-the-badge&logo=google-cloud&logoColor=white" />
  <img src="https://img.shields.io/badge/Microsoft_Azure-0089D6?style=for-the-badge&logo=microsoft-azure&logoColor=white" />
  <img src="https://img.shields.io/badge/Azure_DevOps-0078D7?style=for-the-badge&logo=azure-devops&logoColor=white" />
  <img src="https://img.shields.io/badge/Amazon_AWS-232F3E?style=for-the-badge&logo=amazon-aws&logoColor=white" />
</p>

### CI/CD
<p>
  <img src="https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white">
  <img src="https://img.shields.io/badge/kubernetes-%23326ce5.svg?style=for-the-badge&logo=kubernetes&logoColor=white">  
  <img src="https://img.shields.io/badge/Jenkins-D24939?style=for-the-badge&logo=Jenkins&logoColor=white"/> 
</p>

### Other Things I Work With
<p>
  <img src="https://img.shields.io/badge/GIT-E44C30?style=for-the-badge&logo=git&logoColor=white">
  <img src="https://img.shields.io/badge/Visual_Studio_Code-0078D4?style=for-the-badge&logo=visual%20studio%20code&logoColor=white" />
  <img src="https://img.shields.io/badge/Visual_Studio-5C2D91?style=for-the-badge&logo=visual%20studio&logoColor=white" /> 
  <img src="https://img.shields.io/badge/Jira-0052CC?style=for-the-badge&logo=Jira&logoColor=white" />
  <img src="https://img.shields.io/badge/confluence-%23172BF4.svg?style=for-the-badge&logo=confluence&logoColor=white">   
</p>

## 📞 Contact Me 
<a href="mailto:zhenghoulow@gmail.com">Email Me </a><br> 
<a href="https://www.linkedin.com/in/euanzhlow/"> My Linkedin Page </a> <br>




<!---
💻 Portfolio: [Your Portfolio Website (if applicable)]
euanzhlow/euanzhlow is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
